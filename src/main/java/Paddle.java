import java.awt.*;

public class Paddle {

	public int x;
	public int y;
	public int width = 50;
	public int height = 300;
	public int score;
	public int speed = 10;
	public int puddleNumber;


	public Paddle(Pong pong, int puddleNumber) {

		this.puddleNumber = puddleNumber;

		if (puddleNumber == 1){
			this.x = 0;
		}

		if (puddleNumber == 2){
			this.x = pong.width - width;
		}

		this.y = pong.height / 2 - this.height / 2;

	}

	public void render(Graphics g){
		g.setColor(Color.green);
		g.fillRect(x, y, width, height);
	}

	public void move(boolean up){

		if(up){
			if (y - speed > 0){
				y-=speed;
			}else{
				y = 0;
			}
		}else{
			if (y + height + speed < Pong.pong.height){
				y+=speed;
			}else{
				y = Pong.pong.height - height;
			}
		}
	}


}
