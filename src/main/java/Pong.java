import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

public class Pong implements ActionListener, KeyListener{

	public static Pong pong;
	public int width = 800;
	public int height = 800;
	public Renderer renderer;
	public Paddle player1;
	public Paddle player2;
	public Ball ball;
	public boolean bot = false;
	public boolean w;
	public boolean s;
	public boolean up;
	public boolean down;
	public int gameStatus = 0;
	public Random random;
	public int botMovies;
	public int botCoolDown = 0;
	public int levelBot;
	public boolean selectLevelBot;
	public String levelB;
	public int scoreLimit = 20;
	int playerWin;

	public Pong(){
		Timer timer = new Timer(20, this);

		JFrame jframe = new JFrame("Pong");
		random = new Random();
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.setSize(width + 5, height + 28);  //TODO убрать магичесике числа и сделать размеры окна с учетом ширины бордеров
		renderer = new Renderer();
		jframe.setResizable(false);
		jframe.setLocationRelativeTo(null);
		jframe.setVisible(true);
		jframe.add(renderer);
		jframe.addKeyListener(this);
		timer.start();
	}

	public void start(){
		gameStatus = 2;
		player1 = new Paddle(this, 1);
		player2 = new Paddle(this, 2);
		ball = new Ball(this);
	}

	public void update(){

		if (player1.score >= scoreLimit){
			playerWin = 1;
			gameStatus = 3;
		}
		if (player2.score >= scoreLimit){
			playerWin = 2;
			gameStatus = 3;
		}

		if (w){
			player1.move(true);
		}

		if (s){
			player1.move(false);
		}

		if (!bot){
			if (up){
				player2.move(true);
			}

			if (down){
				player2.move(false);
			}
		}else {
			if (botCoolDown > 0){
				botCoolDown--;
				if (botCoolDown == 0){
					botMovies = 0;
				}
			}
			if(botMovies < 10 ){
				if (player1.y + player2.height / 2 < ball.y){
					player2.move(false);
					botMovies++;
				}
				if (player1.y + player2.height / 2 > ball.y){
					player2.move(true);
					botMovies++;
				}

				if (levelBot == 0){
					botCoolDown = 25;
				}
				if (levelBot == 1){
					botCoolDown = 15;
				}
				if (levelBot == 2){
					botCoolDown = 10;
				}
			}
		}

		ball.update(player1, player2);
	}

	public void render(Graphics2D g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		if (gameStatus == 0){
			g.setColor(Color.green);
			if (!selectLevelBot) {
				g.setFont(new Font("Arial", 1, 30));
				g.drawString("PONG", width / 2 - 45, height / 2 - 10);
				g.setFont(new Font("Arial", 1, 10));
				g.drawString("Press Space to Start", width / 2 - 52, height / 2 + 20);
				g.drawString("Press Shift to Start with Bot", width / 2 - 70, height / 2 + 40);
				g.drawString("Score Limit:  < " + scoreLimit + " >", width / 2 - 47, height / 2 + 60);
			}

			if (selectLevelBot){
				g.setColor(Color.green);
				g.setFont(new Font("Arial", 1, 20));
				levelB = levelBot == 0 ? "Easy" : (levelBot == 1 ? "Medium" : "Hard") ;
				g.drawString("Bot Level:  < " + levelB + " >", width / 2 - 65, height / 2 - 10);
				g.setFont(new Font("Arial", 1, 15));
				g.drawString("Press Space to Start", width / 2 - 52, height / 2 + 20);
			}
		}

		if (gameStatus == 1){
			g.setColor(Color.green);
			g.setFont(new Font("Arial", 1, 30));
			g.drawString("Paused", width / 2 - 55, height / 2 - 30);
		}

		if (gameStatus == 1 || gameStatus == 2){
			g.setColor(Color.green);
			g.setStroke(new BasicStroke(2f));
			g.drawLine(width / 2, 0, width / 2, height);
			g.drawOval(width / 2 - 50, height / 2 - 50, 100, 100);

			g.setFont(new Font("Arial", 1, 30));
			g.drawString(String.valueOf(player1.score), width / 2 - 45, 50);
			g.drawString(String.valueOf(player2.score), width / 2 + 25, 50);

			player1.render(g);
			player2.render(g);
			ball.render(g);
		}

		if (gameStatus == 3){
			g.setColor(Color.green);
			g.setFont(new Font("Arial", 1, 30));
			g.drawString("PONG", width / 2 - 45, height / 2 - 10);

			g.setFont(new Font("Arial", 1, 10));

			if (!bot){
				g.drawString("Thr Bot wins!", width / 2 - 35, height / 2 + 20);
			}else{
				g.drawString("Player " + playerWin + " win!", width / 2 - 35, height / 2 + 20);
			}

			g.drawString("Press Space to Play Again", width / 2 - 65, height / 2 + 40);
			g.drawString("Press ESC for menu", width / 2 - 55, height / 2 + 60);
		}
	}

	public void actionPerformed(ActionEvent e) {

		if (gameStatus == 2){
			update();
		}

		renderer.repaint();
	}

	public static void main(String[] args) {
		pong = new Pong();
	}

	public void keyTyped(KeyEvent e) {

	}

	public void keyPressed(KeyEvent e) {

		int id = e.getKeyCode();

		if (id == KeyEvent.VK_W){
			w = true;
		}else if (id == KeyEvent.VK_S){
			s = true;
		}else if (id == KeyEvent.VK_UP){
			up = true;
		}else if (id == KeyEvent.VK_DOWN){
			down = true;
		}


		else if (id == KeyEvent.VK_RIGHT){
			if (selectLevelBot) {
				if (levelBot < 2) {
					levelBot++;
				} else {
					levelBot = 0;
				}
			}else if (gameStatus == 0) {
				scoreLimit++;
			}
		}else if (id == KeyEvent.VK_LEFT){
			if (selectLevelBot){
				if (levelBot > 0){
					levelBot--;
				}else{
					levelBot = 2;
				}
			}else if (gameStatus == 0 && scoreLimit > 0){
				scoreLimit--;
			}
		}


		else if (id == KeyEvent.VK_ESCAPE && gameStatus == 2 || gameStatus == 3){
			gameStatus = 0;
		}else if (id == KeyEvent.VK_SHIFT && gameStatus == 0){
			bot = true;
			selectLevelBot = true;
		}else if (id == KeyEvent.VK_SPACE){

			if (gameStatus == 0 || gameStatus == 3){
				if (!selectLevelBot){
					bot = false;
				}else {
					selectLevelBot = false;
				}

				start();

			}else if (gameStatus == 1){
				gameStatus = 2;
			}else if (gameStatus == 2){
				gameStatus = 1;
			}

		}
	}

	public void keyReleased(KeyEvent e) {

		int id = e.getKeyCode();

		if (id == KeyEvent.VK_W){
			w = false;
		}

		if (id == KeyEvent.VK_S){
			s = false;
		}

		if (id == KeyEvent.VK_UP){
			up = false;
		}

		if (id == KeyEvent.VK_DOWN){
			down = false;
		}


	}
}
